package com.utp.test.service;

import com.utp.test.model.entity.Note;
import com.utp.test.model.request.NoteRequest;

public interface NoteService {

    public Note save(NoteRequest request) throws Exception;

    public Note findNotesByUserId(Long userId);
}
