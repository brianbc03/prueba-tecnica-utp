package com.utp.test.model.entity;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="notes")
public class Note implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "note_id", nullable = false)
    private Long id;

    @Column(name = "note1")
    private Integer note1;

    @Column(name = "note2")
    private Integer note2;

    @Column(name = "note3")
    private Integer note3;

    @ManyToMany(mappedBy = "noteEntities")
    private List<User> usuarioEntities;

}
