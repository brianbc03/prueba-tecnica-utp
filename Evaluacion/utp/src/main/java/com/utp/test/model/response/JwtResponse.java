package com.utp.test.model.response;

import lombok.*;

@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponse {

    private String token;

}
