package com.utp.test.controller;


import com.utp.test.model.entity.Note;
import com.utp.test.model.request.NoteRequest;
import com.utp.test.service.NoteService;
import com.utp.test.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notes")
@CrossOrigin("*")
public class NotesController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private NoteService noteService;

    @PostMapping("/")
    public Note saveNotes(@RequestBody NoteRequest request) throws Exception{
        return noteService.save(request);
    }

    @GetMapping("/user/{userId}")
    public Note findNotesByUserId(@PathVariable("userId") Long userId){
        return noteService.findNotesByUserId(userId);
    }

}
