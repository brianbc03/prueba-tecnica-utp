package com.utp.test.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.utp.test.model.Authority;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;

import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Setter
@Getter
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="users")
public class User implements UserDetails {

	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "nombre")
	private String name;

	@Column(name = "apellido")
	private String lastName;

	@Column(name = "email")
	private String email;

	@Column(name = "enabled")
	private boolean enabled = true;

	@JoinTable(
			name = "user_notes",
			joinColumns = @JoinColumn(name = "id", nullable = false),
			inverseJoinColumns = @JoinColumn(name="note_id", nullable = false)
	)
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Note> noteEntities;

	@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER,mappedBy = "user")
	@JsonIgnore
	private Set<UserRol> UserRols = new HashSet<>();

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<Authority> autoridades = new HashSet<>();
		this.UserRols.forEach(userRol -> {
			autoridades.add(new Authority(userRol.getRol().getRolNombre()));
		});
		return autoridades;
	}
}
