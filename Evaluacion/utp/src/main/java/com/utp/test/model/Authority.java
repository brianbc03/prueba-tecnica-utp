package com.utp.test.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

@Data
@Setter
@Getter
@AllArgsConstructor
public class Authority implements GrantedAuthority {

    private String authority;

}
