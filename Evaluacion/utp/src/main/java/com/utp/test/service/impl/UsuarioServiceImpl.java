package com.utp.test.service.impl;

import com.utp.test.model.entity.User;
import com.utp.test.model.entity.UserRol;
import com.utp.test.repositories.RolRepository;
import com.utp.test.repositories.UsuarioRepository;
import com.utp.test.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private RolRepository rolRepository;

    @Override
    public User saveUser(User usuario, Set<UserRol> usuarioRoles) throws Exception {
        User usuarioLocal = usuarioRepository.findByUsername(usuario.getUsername());
        if(usuarioLocal != null){
            throw new Exception("El usuario ya existe");
        }
        else{
            for(UserRol usuarioRol:usuarioRoles){
                rolRepository.save(usuarioRol.getRol());
            }
            usuario.getUserRols().addAll(usuarioRoles);
            usuarioLocal = usuarioRepository.save(usuario);
        }
        return usuarioLocal;
    }

    @Override
    public User getUser(String username) {
        return usuarioRepository.findByUsername(username);
    }

    @Override
    public void deleteUser(Long usuarioId) {
        usuarioRepository.deleteById(usuarioId);
    }

    @Override
    public User findById(Long usuarioId) {
        return usuarioRepository.findById(usuarioId).get();
    }

}