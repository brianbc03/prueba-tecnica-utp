package com.utp.test.model.request;

import lombok.*;

@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class JwtRequest {

    private String username;
    private String password;

}
