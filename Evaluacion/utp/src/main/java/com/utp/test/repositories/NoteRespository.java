package com.utp.test.repositories;

import com.utp.test.model.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface NoteRespository extends JpaRepository<Note,Long> {

    @Query(value = "SELECT * FROM notes n, user_notes un WHERE n.note_id= un.note_id and un.id = :userId", nativeQuery = true)
    public Note findNotesByUserId(Long userId);
}
