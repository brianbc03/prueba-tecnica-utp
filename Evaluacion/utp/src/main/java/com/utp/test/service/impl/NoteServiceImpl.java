package com.utp.test.service.impl;

import com.utp.test.model.entity.Note;
import com.utp.test.model.entity.User;
import com.utp.test.model.request.NoteRequest;
import com.utp.test.repositories.NoteRespository;
import com.utp.test.repositories.UsuarioRepository;
import com.utp.test.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteRespository noteRespository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Note save(NoteRequest request) throws Exception {

        User user = usuarioRepository.findById(request.getUserId()).get();
        if(user != null){
            List<User> users = new ArrayList<>();
            users.add(user);
            Note noteEntity = noteRespository.save(Note.builder()
                    .note1(request.getNote1())
                    .note2(request.getNote2())
                    .note3(request.getNote3())
                    .usuarioEntities(users)
                    .build());
            List<Note> noteEntities = new ArrayList<>();
            noteEntities.add(noteEntity);
            user.setNoteEntities(noteEntities);
            usuarioRepository.save(user);
            return noteEntity;

        } else{
            throw new Exception("El usuario no existe");
        }
    }

    @Override
    public Note findNotesByUserId(Long userId) {
        return noteRespository.findNotesByUserId(userId);
    }
}
