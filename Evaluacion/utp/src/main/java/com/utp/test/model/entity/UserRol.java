package com.utp.test.model.entity;

import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@Entity
public class UserRol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userRolId;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @ManyToOne
    private Rol rol;

}
