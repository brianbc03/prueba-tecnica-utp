package com.utp.test.controller;

import com.utp.test.model.entity.Rol;
import com.utp.test.model.entity.User;
import com.utp.test.model.entity.UserRol;
import com.utp.test.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping("/")
    public User saveUser(@RequestBody User user) throws Exception{
        Set<UserRol> usersRoles = new HashSet<>();

        Rol rol = new Rol();
        rol.setRolId(2L);
        rol.setRolNombre("NORMAL");

        UserRol userRol = new UserRol();
        userRol.setUser(user);
        userRol.setRol(rol);

        usersRoles.add(userRol);
        return usuarioService.saveUser(user,usersRoles);
    }


    @GetMapping("/{username}")
    public User getUser(@PathVariable("username") String username){
        return usuarioService.getUser(username);
    }

    @DeleteMapping("/{usuarioId}")
    public void deleteUser(@PathVariable("usuarioId") Long usuarioId){
        usuarioService.deleteUser(usuarioId);
    }


}
