package com.utp.test.model.request;

import lombok.*;

@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class NoteRequest {

    private Long noteId;
    private Integer note1;
    private Integer note2;
    private Integer note3;
    private Long userId;

}
