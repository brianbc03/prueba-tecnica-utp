package com.utp.test.service;

import com.utp.test.model.entity.User;
import com.utp.test.model.entity.UserRol;

import java.util.Set;

public interface UsuarioService {

    public User saveUser(User usuario, Set<UserRol> usuarioRoles) throws Exception;

    public User getUser(String username);

    public void deleteUser(Long usuarioId);
    public User findById(Long userId);
}
