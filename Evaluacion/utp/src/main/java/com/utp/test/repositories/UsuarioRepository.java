package com.utp.test.repositories;

import com.utp.test.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<User,Long> {

    public User findByUsername(String username);

}
